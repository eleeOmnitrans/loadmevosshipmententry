﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;


namespace LoadMevosShipmentEntry
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {

                string inputfilefolder = ConfigurationManager.AppSettings["InputfileFolder"];
                string[] files = Directory.GetFiles(inputfilefolder, "Mevo_*.csv", SearchOption.TopDirectoryOnly);

                SqlConnection cn = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                cn.ConnectionString = ConfigurationManager.ConnectionStrings["AzureAPP"].ConnectionString;
                

                
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "";

                foreach (string filename in files)
                {

                    Console.WriteLine("Reading file {0}", filename);
                    string line;
                    StreamReader sr = new StreamReader(filename);
                    DataTable dt = new DataTable();
                    DataRow dr;
                    int idx = 0;
                    while ((line = sr.ReadLine()) != null)
                    {
                        string[] arrLine = line.Split(',');

                        if (arrLine.Length == 10)
                        {
                            if (idx == 0)
                            {
                                foreach (string columnname in arrLine)
                                {
                                    dt.Columns.Add(columnname, typeof(string));
                                }

                                dt.Columns.Add("Filename", typeof(string));
                                dt.Columns["Filename"].DefaultValue = Path.GetFileName(filename);
                            }
                            else
                            {


                                dr = dt.NewRow();

                                for (int i = 0; i < arrLine.Length; i++)
                                {
                                    dr[i] = arrLine[i].Replace("\"", "").Trim();
                                }

                                dt.Rows.Add(dr);

                                }
                        }
                        idx++;
                    }

                    SqlBulkCopy bulkcopy = new SqlBulkCopy(cn);
                    bulkcopy.BulkCopyTimeout = 9999;
                    bulkcopy.DestinationTableName = "[MEVO].[SBTransaction]";
                    //bulkcopy.ColumnMappings.Add("Filename", "Filename");
                    bulkcopy.WriteToServer(dt);

                }



                cn.Close();
                cn.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
